# AVAILABLE APIs

### Frontend: http://139.180.209.199:4200
### API: http://139.180.209.199:8080

---
All Apis:
- [ ] /v1.0/home
- [x] /v1.0/update
- [x] /v1.0/user/register
- [x] /v1.0/user/login
- [x] /v1.0/user/logout
- [x] /v1.0/user/forgot
- [x] /v1.0/user/resetpass
- [x] /v1.0/user/profile (Pic1)
- [x] /v1.0/user/profile/update (Pic1)
- [x] /v1.0/user/organization (Pic5)
- [x] /v1.0/user/request/organization (Pic1)
- [x] /v1.0/user/update/organization (Pic5)
- [ ] /v1.0/user
- [ ] /v1.0/search
- [ ] /v1.0/tickets
- [x] /v1.0/tickets/purchased (Pic2)
- [x] /v1.0/ticket/<int:ticket_id>/packages/purchased (Pic3)
- [x] /v1.0/code/<int:code_id>/used (Pic4)
- [ ] /v1.0/ticket/cancel
- [ ] /v1.0/ticket/accept
- [x] /v1.0/ticket/cart/add
- [x] /v1.0/ticket/cart/list
- [x] /v1.0/ticket/cart/checkout
- [x] /v1.0/ticket/cart/payment
- [x] /v1.0/organizer/ticket/list (Pic6)
- [x] /v1.0/organizer/ticket/create (Pic7)
- [x] /v1.0/organizer/ticket/<int:ticket_id>/detail (Pic7)
- [x] /v1.0/organizer/ticket/<int:ticket_id>/remove (Pic6)
- [x] /v1.0/organizer/ticket/<int:ticket_id>/packages (Pic8)
- [x] /v1.0/organizer/ticket/<int:ticket_id>/package/create (Pic9)
- [x] /v1.0/organizer/ticket/<int:ticket_id>/package/<int:package_id>/detail (Pic8)
- [x] /v1.0/organizer/ticket/<int:ticket_id>/package/<int:package_id>/remove (Pic8)

### Documents and Resources
- django-restframework: http://www.django-rest-framework.org
- jwt: https://jwt.io
- django jwt: https://github.com/GetBlimp/django-rest-framework-jwt
- Angular: https://angular.io
---

### Sample POST using token (using httpie):

```
http --form POST http://127.0.0.1:8000/api/v1.0/user/logout Authorization:"Token f9a567152f5e62d9fb756f72ee312579f3add132"
```

### /home
<pre>
Url: /api/v1.0/home
Status: Done
Authen: Yes
Access: Both
Allow: GET
Desc: Get homepage info: banner, banner location, promotions ...
</pre>

### /update
<pre>
Url: /api/v1.0/update
Status: Done
Authen: No
Access: Both
Allow: GET
Desc: Check app/frontend version
</pre>

---
# Account

### /user/register
<pre>
Url: /v1.0/user/register
Status: Done
Authen: No
Access: User (user app)
Allow: POST
Desc: Tạo user mới
</pre>

```
RequestBody = {
    "phone" : "01689967593",
    "acckit_token" : "BJOIUWRJKLSKFSJHFSJDFSKFHSDFNBSDMNFSMNF",
    "password": "=AFNJKLEWROIBLLSKDFJ",
    "device": "Android|iOS|Browser",
    "device_id": "2042987",
    "device_token": "BJOIUWRJKLSKFSJHFSJDFSKFHSDFNBSDMNFSMNF"
}
```

### /user/login
<pre>
Url: /v1.0/user/login
Status: Done
Authen: No
Access: Both
Allow: POST
</pre>

```
RequestBody = {
    "phone": "018397984213",
    "password": "=AFNJKLEWROIBLLSKDFJ",
    "device": "Android|iOS|Browser",
    "device_id": "2042987",
    "device_token": "BJOIUWRJKLSKFSJHFSJDFSKFHtạiNBSDMNFSMNF"
}
```

### /user/logout
<pre>
Url: /v1.0/user/logout
Status: Done
Authen: Yes
Access: Both
Allow: POST
</pre>

### /user/forgot
<pre>
Url: /v1.0/user/forgot
Status: Done
Authen: Yes
Access: Both
Allow: POST
</pre>

```
RequestBody = {
    "phone": "01231387384",
    "new_pass": "BJOIUWRJKLSKFS",
    "acckit_token" : "BJOIUWRJKLSKFSJHFSJDFSKFHSDFNBSDMNFSMNF"
}
```

### /user/resetpass
<pre>
Url: /v1.0/user/resetpass
Status: Done
Authen: Yes
Access: Both
Allow: POST
</pre>

```
RequestBody = {
    "old_pass": "=kjfauewrndsvn,vmx,cmvnxv",
    "new_pass": "=wksnmxcvnxkjlsldfjskldfsdf",
}
```

### /user
<pre>
Url: /v1.0/user
Status: Done
Authen: Yes
Access: User
Allow: POST
Desc: Update user information
</pre>

```
RequestBody = {
}
```

### /user/profile
<pre>
Url: /v1.0/user/profile
Status: Done
Authen: Yes
Access: Both
Allow: GET
</pre>

```
RequestBody = {
}
```

### /user/profile/update
<pre>
Url: /v1.0/profile/update
Status: Done
Authen: Yes
Access: Both
Allow: POST
</pre>

```
RequestBody = {
	"fullname": "Tyga",
	"email": "tyga@gmail.com",
	"phone": "+84999111222",
	"birthday": "03-06-1994"
}
```

### /user/organization
<pre>
Url: /v1.0/user/organization
Status: Done
Authen: Yes
Access: Both
Allow: GET
</pre>

```
RequestBody = {
}
```

### /user/request/organization
<pre>
Url: /v1.0/user/request/organization
Status: Done
Authen: Yes
Access: Both
Allow: POST
</pre>

```
RequestBody = {
}
```

### /user/update/organization
<pre>
Url: /v1.0/user/update/organization
Status: Done
Authen: Yes
Access: Both
Allow: POST
</pre>

```
RequestBody = {
    "name": "Tyga",
	"email": "tyga@gmail.com",
	"phone": "+84999111222",
	"website": "http://abc.com",
	"description": ""
}
```

# Search

### /search
<pre>
Url: /v1.0/search
Status: Done
Authen: Yes
Access: User
Allow: POST
</pre>

```
RequestBody = {
}
```

# Ticket

### /tickets
<pre>
Url: /v1.0/tickets
Status: Done
Authen: Yes
Access: User
Allow: POST
Desc: Get tickets info
</pre>

```
RequestBody = {
}
```

### /tickets/purchased
<pre>
Url: /v1.0/tickets/purchased
Status: Done
Authen: Yes
Access: User
Allow: GET
Desc: Get tickets purchased info
</pre>

```
RequestBody = {
}
```

### /v1.0/ticket/<int:ticket_id>/packages/purchased
<pre>
Url: /v1.0/ticket/<int:ticket_id>/packages/purchased
Status: Done
Authen: Yes
Access: User
Allow: GET
Desc: Get packages purchased info by ticket
</pre>

```
RequestBody = {
}
```

### /v1.0/code/<int:code_id>/used
<pre>
Url: /v1.0/code/<int:code_id>/used
Status: Done
Authen: Yes
Access: User
Allow: POST
Desc: Used code
</pre>

```
RequestBody = {
    "code": "string"
}
```



### /v1.0/organizer/ticket/list
<pre>
Url: /v1.0/organizer/ticket/list
Status: Done
Authen: Yes
Access: User
Allow: GET
Desc: Get tickets
</pre>

```
RequestBody = {
}
```

### /v1.0/organizer/ticket/create
<pre>
Url: /v1.0/organizer/ticket/create
Status: Done
Authen: Yes
Access: User
Allow: POST
Desc: Create ticket
</pre>

```
RequestBody = {
	"ticket_id": 5, // truyền 0 để tạo mới, truyền số khác 0 để update
	"title": "This is event name",
	"start_at": "1994-06-03 19:00",
	"description": "",
	"content": "",
	"faq": "",
	"tnc": "",
	"state": "PUBLIC",
	"ads_start_at": "1994-06-03 19:00",
	"ads_end_at": "1994-06-03 19:00",
	"is_default_fee": true,
	"category_id": 0,
	"location_id": 0,
	"ads_total": 0
}
```

### /v1.0/organizer/ticket/<int:ticket_id>/detail
<pre>
Url: /v1.0/organizer/ticket/<int:ticket_id>/detail
Status: Done
Authen: Yes
Access: User
Allow: GET
Desc: Get ticket detail
</pre>

```
RequestBody = {
}
```

### /v1.0/organizer/ticket/<int:ticket_id>/remove
<pre>
Url: /v1.0/organizer/ticket/<int:ticket_id>/remove
Status: Done
Authen: Yes
Access: User
Allow: GET
Desc: Remove ticket
</pre>

```
RequestBody = {
}
```

### /v1.0/organizer/ticket/<int:ticket_id>/packages
<pre>
Url: /v1.0/organizer/ticket/<int:ticket_id>/packages
Status: Done
Authen: Yes
Access: User
Allow: GET
Desc: Get packages info by ticket
</pre>

```
RequestBody = {
}
```

### /v1.0/organizer/ticket/<int:ticket_id>/package/create
<pre>
Url: /v1.0/organizer/ticket/<int:ticket_id>/package/create
Status: Done
Authen: Yes
Access: User
Allow: POST
Desc: Create package
</pre>

```
RequestBody = {
	"package_id": 0,  // 0 for create, and different 0 for update
	"title": "This is package 44",
	"price": 50.5,
	"quantity": 111,
	"content": ""
}
```

### /v1.0/organizer/ticket/<int:ticket_id>/package/<int:package_id>/detail
<pre>
Url: /v1.0/organizer/ticket/<int:ticket_id>/package/<int:package_id>/detail
Status: Done
Authen: Yes
Access: User
Allow: GET
Desc: Get package detail
</pre>

```
RequestBody = {
}
```

### /v1.0/organizer/ticket/<int:ticket_id>/package/<int:package_id>/remove
<pre>
Url: /v1.0/organizer/ticket/<int:ticket_id>/package/<int:package_id>/remove
Status: Done
Authen: Yes
Access: User
Allow: POST
Desc: Remove package
</pre>

```
RequestBody = {
}
```

### /ticket
<pre>
Url: /v1.0/ticket?ticket_id=1
Status: Done
Authen: Yes
Access: User
Allow: GET
Desc: Get ticket info
</pre>


```
RequestBody = {
}
```

### /ticket/cancel
<pre>
Url: /v1.0/ticket/cancel
Status: Done
Authen: Yes
Access: User
Allow: GET
Desc: Cancel a ticket
</pre>

```
RequestBody {
    ticket_id: String
}
```

# Khác

### /schedules
<pre>
Url: /v1.0/schedules
Status: Done
Authen: Yes
Access: Staff
Allow: POST
Desc: Update events's schedules
</pre>

```
RequestBody = {
    "start_at": "0",
    "end_at": "24"
}
```

# Cart Flow
#### Step 1: gọi vào `/api/v1.0/ticket/cart/add` để thêm ticket vào giỏ hàng với:
- Method: `POST`
- Request Body:
```
{
    "tickets": [
        {
            'ticket_id': integer,
            'package_id': integer,
            'quantity': integer,
            'type': string // optional
        }
    ]
}
```
`quantity`: truyền 0 để xóa giỏ hàng
`type: update`: để update số lượng trên giỏ hàng
- Response nếu thành công:
```
{
    "code": 200,
    "message": "Ticket has been add to cart",
    "data": {
        "sub_total": 200.0,
        "tax": 10,
        "total": 220.0,
        "tickets": [
            {
                "package_id": 2,
                "ticket_id": "1",
                "name": "Package 2",
                "quantity": 4,
                "price": 50.0
            }
        ],
        "coupon": {}
    }
}
```

#### Step 2: gọi vào `/api/v1.0/ticket/cart/list` để lấy danh sách ticket trong giỏ hàng với:
- Dùng để khi refresh lại trang thì giỏ hàng vẫn còn
- Method: `GET`
- Response:
```
{
    "code": 200,
    "message": "Successfully",
    "data": {
        "sub_total": 200.0,
        "tax": 10,
        "total": 220.0,
        "tickets": [
            {
                "package_id": 2,
                "ticket_id": "1",
                "name": "Package 2",
                "quantity": 4,
                "price": 50.0
            }
        ],
        "coupon": {}
    }
}
```

#### Step 3. gọi vào `/api/v1.0/ticket/cart/checkout` để checkout với:
- Method: `POST`
- Headers:
```
"Authorization": "Token [token_string]" 
```
`token_string` lấy ở `/api/v1.0/user/login`
- Response:
`code: 201` giỏ hàng rỗng nên quay về trang danh sách tickets, không cho người dùng vào checkout
```
{
    "code": 200,
    "message": "Successfully",
    "data": {
        "sub_total": 200.0,
        "tax": 10,
        "total": 220.0,
        "tickets": [
            {
                "package_id": 2,
                "ticket_id": "1",
                "name": "Package 2",
                "quantity": 4,
                "price": 50.0
            }
        ],
        "coupon": {}
    }
}
```

#### Step 4. gọi vào `/api/v1.0/ticket/cart/make-payment` để thực hiện thanh toán
- Note: khi make payment thành công thì session giỏ hàng sẽ bị xoá đi
- Method: `POST`
- Headers:
```
"Authorization": "Token [token_string]" 
```
`token_string` lấy ở `/api/v1.0/user/login`
- Request Body:
```
{
	"participants": {
		"ticket_{ID}": {
			"package_{ID}": [
				{
					"name": "Le Duc Tri",
					"contact_number": "0962298121",
					"email": "le.duc.tri.niit@gmail.com",
					"ic_number": "0962298121",
					"emergency_contact": "0962298121",
					"other_info": ["other info 1", "other info 2", "other info 2"]
				}
			]
		}
	}
}

```
`ticket_{ID}`: thay thế {ID} bằng ID của ticket ví dụ: ticket_1, ticket_2...
`package__{ID}`: thay thế {ID} bằng ID của package ví dụ: package_1, package_2...
- Response:
```
{
    "code": 200,
    "message": "Send to account number: 0, total amount: 220.0 for finish purchase.",
    "data": {
        "full_name": "luuquocthang",
        "account_number": 2,
        "address": "daimo",
        "phone_number": "0364384662",
        "total_amount": 220.0,
        "tickets": [
            {
                "package_id": 2,
                "ticket_id": "1",
                "name": "Package 2",
                "quantity": 4,
                "price": 50.0
            }
        ]
    }
}
```
`code: 201` giỏ hàng rỗng nên quay về trang danh sách tickets, không cho người dùng make payment

# Lỗi:
### Báo request không có session
```
WARNINGS:
?: (1_7.W001) MIDDLEWARE_CLASSES is not set.
	HINT: Django 1.7 changed the global defaults for the MIDDLEWARE_CLASSES. django.contrib.sessions.middleware.SessionMiddleware, django.contrib.auth.middleware.AuthenticationMiddleware, and django.contrib.messages.middleware.MessageMiddleware were removed from the defaults. If your project needs these middleware then you should configure this setting.
```
Fix: mở file `settings.py` sửa `MIDDLEWARE` thành `MIDDLEWARE_CLASSES`

### Lỗi not found module `django.urls`
Fix: mở file `urls.py` comment dòng `from django.urls import path`

### TODO 
- Upload hình
- Lỗi ngày khi insert or update
