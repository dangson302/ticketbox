from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.decorators import authentication_classes, permission_classes
from rest_framework.response import Response
from rest_framework.authtoken.models import Token
from rest_framework.authentication import TokenAuthentication, SessionAuthentication
from rest_framework.permissions import IsAuthenticated
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from apiserver.serializers import ProfileSerializer, OrganizationSerializer

from apiserver.models import Profile
from apiserver.models import City
from apiserver.models import Address
from apiserver.models import ApiVersion
from apiserver.models import Organization

from datetime import datetime
import random

from apiserver.utils import Timestamp
from apiserver.utils import buildProfile
from apiserver.utils import buildOrder
from apiserver.utils import returnForm

from django.conf import settings

import traceback


@api_view(['GET', 'POST'])
@permission_classes([])
@authentication_classes([])
def RegisterViewSet(request):
    if request.method == 'POST':
        """API endpoint that register new user."""
        # verify user input

        if User.objects.filter(username=request.data["username"]).exists():
            return Response(returnForm(400, "Username already registered", None))
        if User.objects.filter(email=request.data["email"]).exists():
            return Response(returnForm(400, "Email already registered", None))

        if not "type" in request.data:
            return Response(returnForm(400, "Invalid input, missing type", None))

        newuser = User.objects.create_user(username=request.data["email"],
                                        email=request.data["email"],
                                        password=request.data["password"])

        newProfile = Profile.objects.create(
            user=newuser,
            usertype=request.data["type"].upper(),
            phone=request.data["phone"],
            location=City.objects.first(),
            address=Address.objects.first()
        )

        # Create default token
        token = Token.objects.create(user=newuser)
        data = {"user": buildProfile(newProfile), "token": token.key}
        return Response(returnForm(200, "success", data))
    else:
        return Response(returnForm(400, "Not support method", None))


@api_view(['GET', 'POST'])
@permission_classes([])
@authentication_classes([])
def LoginViewSet(request):
    """API endpoint that allows version number to be viewed."""
    if request.method == 'POST':
        # {"phone": "016899086561", "password":"taolaadmin", "device_token": "test", "device_id": "my phone", "device": "ios"}
        if not "username" in request.data or not "password" in request.data:
            return Response(returnForm(400, "Invalid input", None))
        user = authenticate(username=request.data["username"], password=request.data["password"])
        if user:
            print("Token renew ...")
            try:
                user.auth_token.delete()
            except Exception as e:
                print("Could not delete token")
            token = Token.objects.get_or_create(user=user)
            profile = Profile.objects.filter(user=user)
            print(token[0].key)
            return Response(returnForm(200, "Success", {"token": token[0].key}))
        else:
            return Response(returnForm(403, "Lỗi đăng nhập, sai tài khoản hoặc mật khẩu", None))
    else:
        return Response(returnForm(400, "Not support method", None))


@api_view(['POST'])
@permission_classes([])
@authentication_classes([])
def ForgotViewSet(request):
    """API endpoint that allows update password."""

    if not "username" in request.data or not "new_pass" in request.data:
        return Response(returnForm(400, "Invalid input", None))

    try:
        # user = User.objects.get(username=request.data["username"])
        # user.set_password(request.data["new_pass"])
        # user.save()
        return Response(returnForm(200, "Process not define", None))
    except:
        return Response(returnForm(401, "Process not define", None))


@api_view(['POST'])
def ResetPassViewSet(request):
    """API endpoint that allows reset password."""

    # {"old_pass":"adminlatao", "new_pass":"taolaadmin"}
    if not "old_pass" in request.data or not "new_pass" in request.data:
        return Response(returnForm(400, "Invalid input", None))
    if not "new_pass" in request.data:
        return Response(returnForm(400, "Invalid input", None))

    #TODO: verify accountkit
    try:
        user = authenticate(username=request.user, password=request.data["old_pass"])
        if not user:
            return Response(returnForm(401, "Không thể thay đổi mật khẩu, sai mật khẩu", None))
        user.set_password(request.data["new_pass"])
        user.save()
        return Response(returnForm(200, "Thay đổi mật khẩu thành công", None))
    except Exception as e:
        return Response(returnForm(401, "Đã có lỗi xảy ra, không thể thay đổi mật khẩu", {"detail": str(e)}))


@api_view(['POST'])
@permission_classes([])
@authentication_classes([])
def LogoutViewSet(request):
    """API endpoint that allows version number to be viewed."""
    if request.method == 'POST':
        print( "Logging out ... ", request.data)
        profile = Profile.objects.filter(id=request.data["user_id"]).first()
        # simply delete the token to force a login
        # request.user.auth_token.delete()
        return Response(returnForm(200, "success", None))


@api_view(['GET', 'POST'])
def CreateSeed(request):

    # name = ['Nguyễn', 'Trần', 'Hoàng', 'Đỗ', 'Phan']
    # name_1 = ['Thị', 'Vân', 'Hải', 'Thu', "Thị Phương", "Hồng"]
    # name_2 = ['Yến', 'Linh', 'Nhung', 'An', 'Huyền', 'Hương']
    name = ['Nguyễn', 'Trần', 'Hoàng', 'Đỗ']
    name_1 = ['Văn', 'Vân']
    name_2 = ['Tuấn', 'Hưng', 'Khắc', 'Đạt', 'Thành', 'Hương']
    ranks = ["A","B","C"]
    avatars = [ "documents/2018/03/19/Model-105061.png",
                "documents/2018/03/19/Model-135020.png",
                "documents/2018/03/19/Model-169908.png",
                "documents/2018/03/19/Model-244371.png",
                "documents/2018/03/19/Model-285284.png",
                "documents/2018/03/19/Model-557843.png",
                "documents/2018/03/19/Model-571169.png",
                "documents/2018/03/19/Model-614503.png",
                "documents/2018/03/19/Model-671557.png",
                "documents/2018/03/19/Model-674268.png",
                "documents/2018/03/19/Model-691919.png",
                "documents/2018/03/19/Model-698874.png",
                "documents/2018/03/19/Model-701747.png",
                "documents/2018/03/19/Model-746386.png",
                "documents/2018/03/19/Model-762528.png",
                "documents/2018/03/19/Model-764880.png",
                "documents/2018/03/19/Model-862517.png",
                "documents/2018/03/19/Model-906024.png",
                "documents/2018/03/19/Model-91224.png",
                "documents/2018/03/19/Model-919607.png"
            ]
    if request.method == 'POST':
        """API endpoint that register new user."""
        for i in range(60,71):
            f_phone = "01689908656{}".format(i)
            f_name = "{} {} {}".format(random.choice(name), random.choice(name_1) ,random.choice(name_2))
            f_uname = "{} {} {}".format(random.choice(name), random.choice(name_1) ,random.choice(name_2))
            f_body = "{},{},{}".format(random.randint(80,110),random.randint(50,70),random.randint(80,110))
            newuser = User.objects.create_user(username=f_phone,email="dangson302@gmail.com",password="taolatester")

            newProfile = Profile.objects.create(
                user=newuser,
                username = f_uname,
                usertype="USER",
                phone=f_phone,
                location=City.objects.first(),
                address=Address.objects.first(),
                fullname = f_name,
                email = "dangson302@gmail.com",
                avatar = random.choice(avatars),
                images = "{},{}".format(random.choice(avatars), random.choice(avatars)),
                description = "Ok",
                rank = random.choice(ranks),
                rating = random.randint(0, 10)/2,
                comment = "No comment",
                note = "No note",
                height = random.randint(300,400)/2,
                weight = random.randint(90,200)/2,
                body = f_body,
                device="Null",
                device_id="Null",
                device_token="Null"
            )

            # Create default token
            token = Token.objects.create(user=newuser)

    profileQuery = Profile.objects.filter()
    for profile in profileQuery:
        # profile.avatar = random.choice(avatars)
        # profile.images = "{},{},{}".format(random.choice(avatars), random.choice(avatars), random.choice(avatars))
        # profile.birthday = datetime.fromtimestamp(659373615 + random.randint(0,30)*1000000 + random.randint(0,20)*10000000)
        # profile.schedule = "8:12,13:18"
        profile.avatar = random.choice(avatars)
        # profile.avatar = str(profile.avatar).replace("/documents","documents")
        print(profile.avatar)
        # profile.rating = random.randint(1, 6)
        profile.save()

        # schedule = []
        # if not profile.schedule == "":
        #     for duration in profile.schedule.split(","):
        #         s, e = duration.split(":")
        #         schedule.append({"start_at":s, "end_at":e, "status": "free"})
        #     print(schedule)

        #     db.child("/staffs").child(profile.id).set({"schedules": schedule })

    return Response(returnForm(200, "success", None))


@api_view(['GET'])
@permission_classes([IsAuthenticated])
@authentication_classes([TokenAuthentication, SessionAuthentication])
def ProfileViewSet(request):
    """API endpoint for get personal profile."""
    try:
        user_id = request.user.id
        user_obj = User.objects.filter(id=user_id).first()
        if not user_obj:
            return Response(returnForm(401, "User ID: {} not found".format(user_id), None))

        resp_data = {
            'name': '',
            'email': '',
            'phone': '',
            'dob': ''
        }
        try:
            user_profile = user_obj.profile
            resp_data['name'] = user_profile.fullname
            resp_data['email'] = user_profile.email
            resp_data['phone'] = user_profile.phone
            resp_data['dob'] = user_profile.birthday if user_profile.birthday else ''
        except ObjectDoesNotExist:
            pass
        return Response(returnForm(200, "Successfully", resp_data))
    except Exception as e:
        traceback.print_exc()
        print("Persional profile ... ", e)
        return Response(returnForm(401, "Something happened, please try again", None))


@api_view(['POST'])
@permission_classes([IsAuthenticated])
@authentication_classes([TokenAuthentication, SessionAuthentication])
def UpdateProfileViewSet(request):
    """API endpoint for update personal profile."""
    try:
        user_id = request.user.id
        user_obj = User.objects.filter(id=user_id).first()
        if not user_obj:
            return Response(returnForm(401, "User ID: {} not found".format(user_id), None))

        _profile = request.data
        valid_profile = ProfileSerializer(data=_profile)
        if not valid_profile.is_valid():
            errors_data = valid_profile.errors
            return Response(returnForm(400, "Invalid input", errors_data))

        try:
            user_obj.profile.fullname = _profile["fullname"]
            user_obj.profile.email = _profile["email"]
            user_obj.profile.phone = _profile["phone"]
            user_obj.profile.birthday = _profile["birthday"]
            user_obj.profile.save()
        except ObjectDoesNotExist:
            user_profile = Profile.objects.create(
                user=user_obj,
                usertype="USER",
                phone=request.data["phone"],
                email=request.data["email"],
                birthday=request.data["birthday"],
            )
            user_profile.save()
        return Response(returnForm(200, "Successfully", _profile))
    except Exception as e:
        traceback.print_exc()
        print("Persional profile ... ", e)
        return Response(returnForm(401, "Something happened, please try again", None))


@api_view(['POST'])
@permission_classes([IsAuthenticated])
@authentication_classes([TokenAuthentication, SessionAuthentication])
def RequestOrganizationViewSet(request):
    """API endpoint for request organization."""
    try:
        user_id = request.user.id
        user_obj = User.objects.filter(id=user_id).first()
        if not user_obj:
            return Response(returnForm(401, "User ID: {} not found".format(user_id), None))

        user_obj.profile.request_organization = True
        user_obj.profile.save()

        return Response(returnForm(200, "Successfully", None))
    except Exception as e:
        traceback.print_exc()
        print("Request organization ... ", e)
        return Response(returnForm(401, "Something happened, please try again", None))


@api_view(['POST'])
@permission_classes([IsAuthenticated])
@authentication_classes([TokenAuthentication, SessionAuthentication])
def UpdateOrganizationViewSet(request):
    """API endpoint for update organization."""
    try:
        user_id = request.user.id
        user_obj = User.objects.filter(id=user_id).first()
        if not user_obj:
            return Response(returnForm(401, "User ID: {} not found".format(user_id), None))

        if not user_obj.profile.request_organization:
            return Response(returnForm(401, "Your account is not Organization", None))

        organization_data = request.data
        valid_data = OrganizationSerializer(data=organization_data)
        if not valid_data.is_valid():
            errors_data = valid_data.errors
            return Response(returnForm(400, "Invalid input", errors_data))

        Organization.objects.update_or_create(user_id=user_id, defaults=organization_data)
        return Response(returnForm(200, "Successfully", organization_data))
    except Exception as e:
        traceback.print_exc()
        print("Update organization ... ", e)
        return Response(returnForm(401, "Something happened, please try again", None))


@api_view(['GET'])
@permission_classes([IsAuthenticated])
@authentication_classes([TokenAuthentication, SessionAuthentication])
def OrganizationViewSet(request):
    """API endpoint for get organization."""
    try:
        user_id = request.user.id
        user_obj = User.objects.filter(id=user_id).first()
        if not user_obj:
            return Response(returnForm(401, "User ID: {} not found".format(user_id), None))

        if not user_obj.profile.request_organization:
            return Response(returnForm(401, "Your account is not Organization", None))

        organization = Organization.objects.filter(user_id=user_id).first()
        if not organization:
            return Response(returnForm(401, "Your account has not updated Organization", None))

        organization_data = {
            'name': organization.name,
            'phone': organization.phone,
            'email': organization.email,
            'website': organization.website,
            'description': organization.description
        }
        return Response(returnForm(200, "Successfully", organization_data))
    except Exception as e:
        traceback.print_exc()
        print("Update organization ... ", e)
        return Response(returnForm(401, "Something happened, please try again", None))
