from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.decorators import authentication_classes, permission_classes
from rest_framework.response import Response

from django.contrib.auth.models import User

from apiserver.models import Order
from apiserver.models import Package
from apiserver.models import Profile
from apiserver.models import OrderHistoryDetails
from apiserver.serializers import TicketSerializer

from apiserver.utils import Timestamp
from apiserver.utils import buildProfile
from apiserver.utils import buildOrder
from apiserver.utils import returnForm
from django.utils import timezone

from datetime import datetime
from dateutil.relativedelta import relativedelta
import json

from urllib.parse import urlencode
from urllib.request import Request, urlopen

from django.conf import settings

from rest_framework.authentication import TokenAuthentication, SessionAuthentication
from rest_framework.permissions import IsAuthenticated

import hashlib
import traceback


@api_view(['GET', 'POST'])
@permission_classes([])
def TicketsView(request):
    """API endpoint that allows version number to be viewed."""
    tickets = Order.objects.all()
    tickets_json = []
    for t in tickets:
        start_at = {}
        category = t.category.name if t.category else ''

        if t.start_at:
            start_at = {
                "date": t.start_at.strftime('%d'),
                "month": t.start_at.strftime('%b')
            }

        tickets_json.append({
            # TODO: Add slug
            "description": t.description,
            "category": category,
            "loc": t.location.name if t.location else '',
            "start_at": start_at,
            "total": t.total,
            "id": t.id
        })
    return Response(returnForm(200, "Success", tickets_json))


@api_view(['GET', 'POST'])
@permission_classes([])
def TicketView(request, ticket_id):
    """API endpoint that allows version number to be viewed."""
    ticket = Order.objects.get(id=ticket_id)
    if ticket:
        ticket_json = {
            # TODO: Add slug
            "title": ticket.title,
            "description": ticket.description,
            "content": ticket.content,
            "loc": ticket.location.name if ticket.location else '',
            "total": ticket.total,
            "id": ticket.id,
            "end_at": ticket.end_at
        }
        packages = Package.objects.filter(ticket=ticket, is_delete=False)
        if packages.count():
            packages_json = []
            for package in packages:
                packages_json.append({
                    "title": package.title,
                    "content": package.content,
                    "price": package.price,
                    "quantity": package.quantity,
                    "left": package.left,
                    "id": package.id
                })
            ticket_json["packages"] = packages_json
    else :
        return Response(returnForm(200, "No ticket found", None))
    return Response(returnForm(200, "Success", ticket_json))


@api_view(['GET'])
@permission_classes([IsAuthenticated])
@authentication_classes([TokenAuthentication, SessionAuthentication])
def TicketsPurchasedViewSet(request):
    try:
        user_id = request.user.id
        user_obj = User.objects.filter(id=user_id).first()
        if not user_obj:
            return Response(returnForm(401, "User ID: {} not found".format(user_id), None))

        resp_data = []
        order_history = OrderHistoryDetails.objects.filter(
            order__user_id=user_id
        ).all()

        tmp = []
        for order in order_history:
            if order.ticket.id not in tmp:
                tmp.append(order.ticket.id)
            else:
                continue

            long_start_at = ''
            short_start_at = ''
            location = order.ticket.location.name if order.ticket.location else ''
            category = order.ticket.category if order.ticket.category else ''
            banner = order.ticket.banner.url if order.ticket.banner else ''

            if order.ticket.start_at:
                long_start_at = order.ticket.start_at.strftime('%a, %d %b %Y, %-I.%M%p')
                short_start_at = order.ticket.start_at.strftime('%b %d')

            resp_data.append({
                'ticket_id': order.ticket.id,
                'name': order.ticket.title,
                'category': category,
                'long_start_at': long_start_at,
                'short_start_at': short_start_at,
                'location': location,
                'image': banner
            })
        return Response(returnForm(200, "Successfully", resp_data))
    except Exception as e:
        traceback.print_exc()
        print("Purchased tickets ... ", e)
        return Response(returnForm(401, "Something happened, please try again", None))


@api_view(['GET'])
@permission_classes([IsAuthenticated])
@authentication_classes([TokenAuthentication, SessionAuthentication])
def OrganizerTicketsViewSet(request):
    try:
        user_id = request.user.id
        user_obj = User.objects.filter(id=user_id).first()
        if not user_obj:
            return Response(returnForm(401, "User ID: {} not found".format(user_id), None))

        resp_data = []
        ticket_objs = Order.objects.filter(
            user_id=user_id, is_delete=False
        ).all()

        for obj in ticket_objs:
            long_start_at = ''
            short_start_at = ''
            location = obj.location.name if obj.location else ''
            category = obj.category if obj.category else ''
            banner = obj.banner.url if obj.banner else ''

            if obj.start_at:
                long_start_at = obj.start_at.strftime('%a, %d %b %Y, %-I.%M%p')
                short_start_at = obj.start_at.strftime('%b %d')

            resp_data.append({
                'ticket_id': obj.id,
                'name': obj.title,
                'category': category,
                'long_start_at': long_start_at,
                'short_start_at': short_start_at,
                'location': location,
                'banner': banner
            })
        return Response(returnForm(200, "Successfully", resp_data))
    except Exception as e:
        traceback.print_exc()
        print("Organizer tickets ... ", e)
        return Response(returnForm(401, "Something happened, please try again", None))


@api_view(['GET'])
@permission_classes([IsAuthenticated])
@authentication_classes([TokenAuthentication, SessionAuthentication])
def OrganizerTicketDetailViewSet(request, ticket_id):
    try:
        user_id = request.user.id
        user_obj = User.objects.filter(id=user_id).first()
        if not user_obj:
            return Response(returnForm(401, "User ID: {} not found".format(user_id), None))

        ticket_obj = Order.objects.filter(id=ticket_id, user_id=user_id, is_delete=False).first()
        if not ticket_obj:
            return Response(returnForm(401, "Ticket ID: {} not found".format(ticket_id), None))

        long_start_at = ticket_obj.start_at.strftime('%d %b %Y, %-I.%M%p') if ticket_obj.start_at else ''
        location = ticket_obj.location.name if ticket_obj.location else ''
        content = ticket_obj.content if ticket_obj.content else ''
        description = ticket_obj.description if ticket_obj.description else ''
        category = ticket_obj.category if ticket_obj.category else ''
        state = dict(Order.STATUS)[ticket_obj.state] if ticket_obj.state else ''
        content_image = ticket_obj.content_image.url if ticket_obj.content_image else ''
        banner = ticket_obj.banner.url if ticket_obj.banner else ''
        faq = ticket_obj.faq if ticket_obj.faq else ''
        tnc = ticket_obj.tnc if ticket_obj.tnc else ''
        ads_start_date = ticket_obj.ads_start_at.strftime('%d %b %Y') if ticket_obj.ads_start_at else ''
        ads_end_date = ticket_obj.ads_start_at.strftime('%d %b %Y') if ticket_obj.ads_end_at else ''
        ads_start_time = ticket_obj.ads_start_at.strftime('%-I.%M%p') if ticket_obj.ads_start_at else ''
        ads_end_time = ticket_obj.ads_start_at.strftime('%-I.%M%p') if ticket_obj.ads_end_at else ''
        ads_total = ticket_obj.ads_total

        resp_data = {
            'ticket_id': ticket_obj.id,
            'name': ticket_obj.title,
            'description': description,
            'content': content,
            'category': category,
            'start_at': long_start_at,
            'location': location,
            'banner': banner,
            'content_image': content_image,
            'status': state,
            'faq': faq,
            'tnc': tnc,
            'is_default_fee': ticket_obj.is_default_fee,
            'ads_start_date': ads_start_date,
            'ads_end_date': ads_end_date,
            'ads_start_time': ads_start_time,
            'ads_end_time': ads_end_time,
            'ads_total': ads_total
        }
        return Response(returnForm(200, "Successfully", resp_data))
    except Exception as e:
        traceback.print_exc()
        print("Organizer ticket detail ... ", e)
        return Response(returnForm(401, "Something happened, please try again", None))


@api_view(['POST'])
@permission_classes([IsAuthenticated])
@authentication_classes([TokenAuthentication, SessionAuthentication])
def OrganizerTicketCreateUpdateViewSet(request):
    try:
        user_id = request.user.id
        user_obj = User.objects.filter(id=user_id).first()
        if not user_obj:
            return Response(returnForm(401, "User ID: {} not found".format(user_id), None))

        ticket_data = request.data
        valid_package = TicketSerializer(data=ticket_data)
        if not valid_package.is_valid():
            errors_data = valid_package.errors
            return Response(returnForm(400, "Invalid input", errors_data))

        ticket_id = ticket_data["ticket_id"] if ticket_data["ticket_id"] else None
        ticket_data.pop("ticket_id")

        if not ticket_data["category_id"]:
            ticket_data.pop("category_id")
        if not ticket_data["location_id"]:
            ticket_data.pop("location_id")

        ticket_data.update({
            'user_id': user_id,
            'end_at': ticket_data['start_at']
        })

        Order.objects.update_or_create(id=ticket_id, defaults=ticket_data)
        return Response(returnForm(200, "Successfully", None))
    except Exception as e:
        traceback.print_exc()
        print("Organizer ticket create update ... ", e)
        return Response(returnForm(401, "Something happened, please try again", None))


@api_view(['POST'])
@permission_classes([IsAuthenticated])
@authentication_classes([TokenAuthentication, SessionAuthentication])
def OrganizerTicketRemoveViewSet(request, ticket_id):
    try:
        user_id = request.user.id
        user_obj = User.objects.filter(id=user_id).first()
        if not user_obj:
            return Response(returnForm(401, "User ID: {} not found".format(user_id), None))

        ticket_obj = Order.objects.filter(id=ticket_id, user_id=user_id, is_delete=False).first()
        if not ticket_obj:
            return Response(returnForm(401, "Ticket ID: {} not found".format(user_id), None))

        ticket_obj.is_delete = True
        ticket_obj.save()

        return Response(returnForm(200, "Successfully", None))
    except Exception as e:
        traceback.print_exc()
        print("Organizer ticket remove ... ", e)
        return Response(returnForm(401, "Something happened, please try again", None))
