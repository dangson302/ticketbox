from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.decorators import authentication_classes, permission_classes
from rest_framework.response import Response

from django.contrib.auth.models import User
from apiserver.models import Profile

from apiserver.utils import Timestamp
from apiserver.utils import buildProfile
from apiserver.utils import buildOrder
from apiserver.utils import returnForm

import time
from datetime import datetime
from dateutil.relativedelta import relativedelta

def Search(request, limit=None):

    # {"rate":5,"height_min":100,"height_max":200,"weight_min":40,"weight_max":100}
    filter = Profile.objects.filter()
    if not request.data:
        filter = filter.filter(usertype="STAFF").order_by('-id')[:20]
    else:
        if "rate" in request.data:
            filter = filter.filter(rating=float(request.data["rate"]))

        if "height_min" in request.data:
            filter = filter.filter(height__gte=float(request.data["height_min"]))

        if "height_max" in request.data:
            filter = filter.filter(height__lte=float(request.data["height_max"]))

        if "weight_min" in request.data:
            filter = filter.filter(weight__gte=float(request.data["weight_min"]))

        if "weight_max" in request.data:
            filter = filter.filter(weight__lte=float(request.data["weight_max"]))

        if "location_id" in request.data:
            filter = filter.filter(location__id=int(request.data["location_id"]))

    print( "Search query:", request.data)

    if "age_min" in request.data:
        #{"age_min":30}
        current_time = starting_day_of_current_year = datetime.now()
        age_min = int(request.data["age_min"])
        print("Current server time:", current_time)
        age_min_time = current_time - relativedelta(years=+age_min)
        print("Birthday must after:", age_min_time)
        filter = filter.filter(birthday__lte=age_min_time)

    if "age_max" in request.data:
        #{"age_max":30}
        current_time = starting_day_of_current_year = datetime.now()
        age_max = int(request.data["age_max"])
        print("Current server time:", current_time)
        age_max_time = current_time - relativedelta(years=+age_max)
        print("Birthday must before:", age_max_time)
        filter = filter.filter(birthday__gte=age_max_time)

    # if "age_max" in request.data:
    #     filter = filter.filter(weight<request.data["age_max"])

    if limit:
        filter = filter[:1]
    else:
        if "total" in request.data and not request.data["total"] in [0, "0"]:
            filter = filter[:request.data["total"]]

    if not filter:
        return Response(returnForm(404, "Không tìm thấy nhân viên như yêu cầu", None))

    data = {"staffs" : []}
    for account in filter:

        if "start_at" in request.data:
            _startHour = datetime.fromtimestamp(float(request.data["start_at"])).hour

            if "end_at" in request.data:
                _endHour = datetime.fromtimestamp(float(request.data["end_at"])).hour

                if account.isFree(_startHour, _endHour):
                    data["staffs"].append(buildProfile(account))
            else:
                return Response(returnForm(400, "Yêu cầu điền đầy đủ thông tin giờ bắt đầu và giờ kết thúc", None))

        else:
            data["staffs"].append(buildProfile(account))
    if data["staffs"]:
        return Response(returnForm(200, "", data))
    else:
        return Response(returnForm(201, "Không có kết quả phù hợp với tìm kiếm của bạn", None))

@api_view(['GET','POST'])
def SearchMany(request):
    """API endpoint that allows version number to be viewed."""
    return Search(request)
