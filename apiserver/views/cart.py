import traceback
import json
from rest_framework.decorators import api_view
from rest_framework.decorators import authentication_classes, permission_classes
from rest_framework.response import Response
from rest_framework.authentication import TokenAuthentication, SessionAuthentication
from rest_framework.permissions import IsAuthenticated
from apiserver.utils import returnForm
from apiserver.models import Order, Package, OrderHistory, OrderHistoryDetails, ParticipantInfo
from apiserver.serializers import AddToCardSerializer, ParticipantInfoSerializer
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.db import transaction
from hashids import Hashids


def get_cart_list(request):
    ss_tickets = request.session.get('tickets') if 'tickets' in request.session else None
    ss_coupon = request.session.get('coupon') if 'coupon' in request.session else None
    tickets = []
    coupon = {}
    sub_total = 0
    tax = 10
    total = 0

    if ss_tickets:
        for ticket in ss_tickets:
            ticket_id = ticket.split('_')[1]

            for package in ss_tickets[ticket]:
                package_id = package.split('_')[1]
                quantity = ss_tickets[ticket][package]
                package_obj = Package.objects.filter(id=package_id, is_delete=False).first()

                if package_obj:
                    sub_total += (quantity * package_obj.price)
                    tickets.append({
                        'package_id': package_obj.id,
                        'ticket_id': ticket_id,
                        'name': package_obj.title,
                        'quantity': quantity,
                        'price': package_obj.price
                    })

    if sub_total:
        total = sub_total + ((sub_total * 10) / 100)

    if ss_coupon:
        pass

    tickets = {
        'sub_total': sub_total,
        'tax': tax,
        'total': total,
        'tickets': tickets,
        'coupon': coupon
    }
    return tickets


@api_view(['POST'])
@permission_classes([])
@authentication_classes([])
def AddToCartViewSet(request):
    """API endpoint add to cart."""
    if 'tickets' not in request.data:
        return Response(returnForm(400, "Invalid input", None))

    tickets = request.data["tickets"]

    for ticket in tickets:
        valid_data = AddToCardSerializer(data=ticket)
        if not valid_data.is_valid():
            errors_data = valid_data.errors
            return Response(returnForm(400, "Invalid input", errors_data))

    try:
        ss_tickets = request.session.get('tickets')

        for ticket in tickets:
            ticket_id = ticket["ticket_id"] if "ticket_id" in ticket else None
            package_id = ticket["package_id"] if "package_id" in ticket else None
            quantity = ticket["quantity"] if "quantity" in ticket else None
            _type = ticket["type"] if "type" in ticket else None

            ticket_obj = Order.objects.filter(id=ticket_id, is_delete=False)
            package_obj = Package.objects.filter(id=package_id, ticket_id=ticket_id, is_delete=False)

            if not ticket_obj or not package_obj:
                error_message = "Ticket ID: {} not found".format(ticket_id)
                if not package_obj:
                    error_message = "Package ID: {} not found".format(package_id)
                return Response(returnForm(400, error_message, None))

            sst_id = 'ticket_{}'.format(ticket_id)
            ssp_id = 'package_{}'.format(package_id)

            if not ss_tickets and quantity > 0:
                ss_tickets = {
                    sst_id: {
                        ssp_id: int(quantity)
                    }
                }
            else:
                if sst_id not in ss_tickets and quantity > 0:
                    ss_tickets[sst_id] = {
                        ssp_id: int(quantity)
                    }
                else:
                    if quantity < 1:
                        ss_tickets[sst_id].pop(ssp_id, None)
                    else:
                        if _type == 'update':
                            ss_tickets[sst_id][ssp_id] = int(quantity)
                        else:
                            ticket_quantity = ss_tickets[sst_id].get(ssp_id, 1)
                            ss_tickets[sst_id][ssp_id] = int(quantity) + int(ticket_quantity)

        request.session['tickets'] = ss_tickets
        resp_data = get_cart_list(request)
        return Response(returnForm(200, "Ticket has been add to cart", resp_data))
    except Exception as e:
        traceback.print_exc()
        print("Add to cart error ... ", e)
        return Response(returnForm(401, "Ticket hasn't been add to cart", None))


@api_view(['GET'])
@permission_classes([])
@authentication_classes([])
def CartListViewSet(request):
    """API endpoint get cart list."""
    try:
        resp_data = get_cart_list(request)
        return Response(returnForm(200, "Successfully", resp_data))
    except Exception as e:
        traceback.print_exc()
        print("Add to cart error ... ", e)
        return Response(returnForm(401, "Something happened, please try again", None))


@api_view(['POST'])
@permission_classes([IsAuthenticated])
@authentication_classes([TokenAuthentication, SessionAuthentication])
def CartCheckoutViewSet(request):
    """API endpoint for checkout."""
    try:
        ss_tickets = request.session.get('tickets')
        if not ss_tickets:
            return Response(returnForm(201, "Your cart was empty!", None))

        resp_data = get_cart_list(request)
        # resp_data.update({
        #     'timeslot': datetime.datetime.now().strftime('%d %B %Y, %-I.%M%p')
        # })
        return Response(returnForm(200, "Successfully", resp_data))
    except Exception as e:
        traceback.print_exc()
        print("Checkout error ... ", e)
        return Response(returnForm(401, "Something happened, please try again", None))


@api_view(['POST'])
@permission_classes([IsAuthenticated])
@authentication_classes([TokenAuthentication, SessionAuthentication])
def CartMakePaymentViewSet(request):
    """API endpoint for make payment."""
    if "participants" not in request.data:
        return Response(returnForm(400, "Invalid input", None))

    try:
        participants = request.data["participants"]
        for participant_info in participants.values():
            for _package_info in participant_info.values():
                for _info in _package_info:
                    valid_participant = ParticipantInfoSerializer(data=_info)
                    if not valid_participant.is_valid():
                        errors_data = valid_participant.errors
                        return Response(returnForm(400, "Invalid input", errors_data))

        ss_tickets = request.session.get('tickets')
        if not ss_tickets:
            return Response(returnForm(201, "Your cart was empty!", None))

        user_obj = User.objects.filter(id=request.user.id).first()
        if not user_obj:
            return Response(returnForm(401, "User ID: {} not found".format(request.user.id), None))

        try:
            user_obj.profile
        except ObjectDoesNotExist:
            return Response(returnForm(401, "User ID: {} not found profile".format(request.user.id), None))

        order = get_cart_list(request)
        account_number = 0
        total_amount = order['total']

        address = ''
        try:
            address = user_obj.profile.address.street
        except ObjectDoesNotExist:
            pass

        resp_data = {
            'full_name': user_obj.profile.username,
            'account_number': user_obj.id,
            'address': address,
            'phone_number': user_obj.profile.phone,
            'total_amount': total_amount,
            'tickets': order['tickets']
        }

        hashid = Hashids(min_length=12)

        with transaction.atomic():
            order_obj = OrderHistory()
            order_obj.user = user_obj
            order_obj.total = total_amount
            order_obj.sub_total = order['sub_total']
            order_obj.tax = order['tax']
            order_obj.coupon_discount = 0
            order_obj.save()

            for ticket in order['tickets']:
                ticket_obj = Order.objects.filter(id=ticket['ticket_id'], is_delete=False).first()
                package_obj = Package.objects.filter(id=ticket['package_id'], is_delete=False).first()

                if not ticket_obj or not package_obj:
                    continue

                ticket_id = 'ticket_{}'.format(ticket_obj.id)
                package_id = 'package_{}'.format(package_obj.id)

                order_details = OrderHistoryDetails()
                order_details.package = package_obj
                order_details.order = order_obj
                order_details.ticket = ticket_obj
                order_details.quantity = ticket['quantity']
                order_details.save()
                order_details.code = hashid.encode(order_details.id)
                order_details.save()

                if ticket_id in participants and package_id in participants[ticket_id]:
                    for _info in participants[ticket_id][package_id]:
                        participant = ParticipantInfo()
                        participant.order_history = order_details
                        participant.participant_name = _info["name"] if "name" in _info else None
                        participant.contact_number = _info["contact_number"] if "contact_number" in _info else None
                        participant.email = _info["email"] if "email" in _info else None
                        participant.ic_number = _info["ic_number"] if "ic_number" in _info else None
                        participant.emergency_contact = _info["emergency_contact"] if "emergency_contact" in _info else None
                        participant.other_info = json.dumps(_info["other_info"] if "other_info" in _info else [])
                        participant.save()

        message = "Send to account number: {}, total amount: {} for finish purchase.".format(
            account_number, total_amount
        )

        if 'tickets' in request.session:
            request.session.pop('tickets')

        if 'coupon' in request.session:
            request.session.pop('coupon')

        return Response(returnForm(200, message, resp_data))
    except Exception as e:
        traceback.print_exc()
        print("Make payment error ... ", e)
        return Response(returnForm(401, "Something happened, please try again", None))


