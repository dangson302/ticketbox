from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.decorators import authentication_classes, permission_classes
from rest_framework.response import Response

from django.contrib.auth.models import User

from apiserver.models import City
from apiserver.models import Address
from apiserver.models import Profile
from apiserver.models import ApiVersion
from apiserver.models import Order
from apiserver.models import Banner
from apiserver.models import City

from apiserver.utils import Timestamp
from apiserver.utils import buildProfile
from apiserver.utils import buildOrder
from apiserver.utils import returnForm

from django.core import serializers

@api_view(['GET'])
@permission_classes([])
def VersionViewSet(request):
    """API endpoint that allows version number to be viewed."""

    if request.method == 'GET':
        version = ApiVersion.objects.all()
        # serializer = VersionSerializer(version, many=True)
        if version.count()!=1:
            return Response(returnForm(400, "Too many version number or no version defined", None))

        versionData = version.first()

        data = {"android": {"latest": versionData.AndroidVersionNumber,
                            "message": versionData.AndroidVersionDetail,
                            "min_user": versionData.minimumUserAndroidAppSupportVersion,
                            "min_staff": versionData.minimumStaffAndroidAppSupportVersion},
                "ios":      {"latest": versionData.IOSVersionNumber,
                            "message": versionData.IOSVersionDetail,
                            "min_user": versionData.minimumUserIOSAppSupportVersion,
                            "min_staff": versionData.minimumStaffIOSAppSupportVersion}
        }

        return Response(returnForm(200, "Has a new version 2, please update", data))

@api_view(['GET'])
@permission_classes([])
def HomeViewSet(request):
    """API endpoint that allows home page data to be viewed."""

    if request.method == 'GET':
        # version = ApiVersion.objects.all()
        # # serializer = VersionSerializer(version, many=True)
        # if version.count()!=1:
        #     return Response(returnForm(400, "Too many version number or no version defined", None))

        # versionData = version.first()

        # profiles = Profile.objects.filter(usertype="STAFF").order_by('-rating')[:5]

        # hot_profiles = []
        # for _profile in profiles:
        #     hot_profiles.append(buildProfile(_profile))

        # profiles = Profile.objects.filter(usertype="STAFF").order_by('-id')[:5]

        # new_profiles = []
        # for _profile in profiles:
        #     new_profiles.append(buildProfile(_profile))
        banners_top = Banner.objects.filter(bannerType="top")
        banner_top_json = []
        for b in banners_top:
            banner_top_json.append({
                "url": b.url,
                "image": b.images
            })

        banners_left = Banner.objects.filter(bannerType="left")
        banner_left_json = []
        for b in banners_left:
            banner_left_json.append({
                "url": b.url,
                "image": b.images
            })

        data = {"banner_top": banner_top_json,
                "banner_left": banner_left_json
        }

        return Response(returnForm(200, "", data))

@api_view(['GET'])
@permission_classes([])
def LocationViewSet(request):
    """API endpoint that allows version number to be viewed."""
    cityQuery = City.objects.filter()
    data = {"locations": []}
    if cityQuery.exists():
        for city in cityQuery.all():
            data["locations"].append({"id": city.id, "name": city.name})

        return Response(returnForm(200, "", data))
    else:
        return Response(returnForm(400, "Chưa setup địa điểm trên server", None))

@api_view(['GET'])
def RedirectedView(request):
    return Response(returnForm(200, "API cũ, chuyển cmn sang đường dẫn khác rồi", None))
