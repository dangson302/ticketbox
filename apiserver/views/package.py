import traceback

from django.contrib.auth.models import User

from rest_framework.decorators import api_view, authentication_classes, \
    permission_classes
from rest_framework.response import Response
from rest_framework.authentication import TokenAuthentication, SessionAuthentication
from rest_framework.permissions import IsAuthenticated

from apiserver.models import Order, Package, OrderHistoryDetails
from apiserver.serializers import PackageSerializer
from apiserver.utils import returnForm


@api_view(['GET'])
@permission_classes([IsAuthenticated])
@authentication_classes([TokenAuthentication, SessionAuthentication])
def PackagesViewSet(request, ticket_id):
    try:
        user_id = request.user.id
        user_obj = User.objects.filter(id=user_id).first()
        if not user_obj:
            return Response(returnForm(401, "User ID: {} not found".format(user_id), None))

        if not ticket_id:
            return Response(returnForm(401, "Invalid input", None))

        ticket_obj = Order.objects.filter(id=ticket_id, is_delete=False).first()
        if not ticket_obj:
            return Response(returnForm(401, "Ticket ID: {} not found".format(ticket_id), None))

        resp_data = {
            'ticket_id': ticket_obj.id,
            'ticket_name': ticket_obj.title,
            'packages': []
        }
        package_objs = Package.objects.filter(ticket_id=ticket_id, is_delete=False).all()
        for package in package_objs:
            resp_data['packages'].append({
                'name': package.title,
                'price': package.price,
                'quantity': package.quantity,
                'description': package.content
            })
        return Response(returnForm(200, "Successfully", resp_data))
    except Exception as e:
        traceback.print_exc()
        print("Packages ... ", e)
        return Response(returnForm(401, "Something happened, please try again", None))


@api_view(['GET'])
@permission_classes([IsAuthenticated])
@authentication_classes([TokenAuthentication, SessionAuthentication])
def PackageDetailViewSet(request, ticket_id, package_id):
    try:
        user_id = request.user.id
        user_obj = User.objects.filter(id=user_id).first()
        if not user_obj:
            return Response(returnForm(401, "User ID: {} not found".format(user_id), None))

        if not ticket_id or not package_id:
            return Response(returnForm(401, "Invalid input", None))

        ticket_obj = Order.objects.filter(id=ticket_id, is_delete=False).first()
        if not ticket_obj:
            return Response(returnForm(401, "Ticket ID: {} not found".format(ticket_id), None))

        package_obj = Package.objects.filter(id=package_id, ticket_id=ticket_id, is_delete=False).first()
        if not package_obj:
            return Response(returnForm(401, "Package ID: {} not found".format(package_id), None))

        resp_data = {
            'id': package_obj.id,
            'title': package_obj.title,
            'price': package_obj.price,
            'quantity': package_obj.quantity,
            'content': package_obj.content
        }
        return Response(returnForm(200, "Successfully", resp_data))
    except Exception as e:
        traceback.print_exc()
        print("Packages ... ", e)
        return Response(returnForm(401, "Something happened, please try again", None))


@api_view(['POST'])
@permission_classes([IsAuthenticated])
@authentication_classes([TokenAuthentication, SessionAuthentication])
def PackageCreateUpdateViewSet(request, ticket_id):
    try:
        user_id = request.user.id
        user_obj = User.objects.filter(id=user_id).first()
        if not user_obj:
            return Response(returnForm(401, "User ID: {} not found".format(user_id), None))

        if not ticket_id:
            return Response(returnForm(401, "Invalid input", None))

        ticket_obj = Order.objects.filter(id=ticket_id, is_delete=False).first()
        if not ticket_obj:
            return Response(returnForm(401, "Ticket ID: {} not found".format(ticket_id), None))

        package_data = request.data
        valid_package = PackageSerializer(data=package_data)
        if not valid_package.is_valid():
            errors_data = valid_package.errors
            return Response(returnForm(400, "Invalid input", errors_data))

        package_id = package_data["package_id"] if package_data["package_id"] else None
        package_data.pop("package_id")
        package_data.update({
            'ticket_id': ticket_id,
            'left': package_data['quantity']
        })
        Package.objects.update_or_create(id=package_id, defaults=package_data)
        return Response(returnForm(200, "Successfully", package_data))
    except Exception as e:
        traceback.print_exc()
        print("Package create update ... ", e)
        return Response(returnForm(401, "Something happened, please try again", None))


@api_view(['POST'])
@permission_classes([IsAuthenticated])
@authentication_classes([TokenAuthentication, SessionAuthentication])
def PackageDeleteViewSet(request, ticket_id, package_id):
    try:
        user_id = request.user.id
        user_obj = User.objects.filter(id=user_id).first()
        if not user_obj:
            return Response(returnForm(401, "User ID: {} not found".format(user_id), None))

        if not ticket_id or not package_id:
            return Response(returnForm(401, "Invalid input", None))

        ticket_obj = Order.objects.filter(id=ticket_id, is_delete=False).first()
        if not ticket_obj:
            return Response(returnForm(401, "Ticket ID: {} not found".format(ticket_id), None))

        package_obj = Package.objects.filter(id=package_id, ticket_id=ticket_id, is_delete=False).first()
        if not package_obj:
            return Response(returnForm(401, "Package ID: {} not found".format(package_id), None))

        package_obj.is_delete = True
        package_obj.save()
        return Response(returnForm(200, "Successfully", None))
    except Exception as e:
        traceback.print_exc()
        print("Package create update ... ", e)
        return Response(returnForm(401, "Something happened, please try again", None))


@api_view(['GET'])
@permission_classes([IsAuthenticated])
@authentication_classes([TokenAuthentication, SessionAuthentication])
def PackagesPurchasedViewSet(request, ticket_id):
    try:
        user_id = request.user.id
        user_obj = User.objects.filter(id=user_id).first()
        if not user_obj:
            return Response(returnForm(401, "User ID: {} not found".format(user_id), None))

        if not ticket_id:
            return Response(returnForm(401, "Invalid input", None))

        ticket_obj = Order.objects.filter(id=ticket_id, is_delete=False).first()
        if not ticket_obj:
            return Response(returnForm(401, "Ticket ID: {} not found".format(ticket_id), None))

        resp_data = []
        order_history = OrderHistoryDetails.objects.filter(
            order__user_id=user_id,
            ticket_id=ticket_obj.id
        ).all()

        tmp = {}
        code_status = dict(OrderHistoryDetails.STATUS)
        for order in order_history:
            expired = ''

            if order.ticket.end_at:
                expired = order.ticket.end_at.strftime('%d %B %Y, %-I.%M%p')

            if order.package.id not in tmp:
                tmp.update({
                    order.package.id: {
                        'package_id': order.package.id,
                        'package_name': order.package.title,
                        'expired': expired,
                        'price': order.package.price,
                        'code': [{
                            'id': order.id,
                            'code': order.code,
                            'status': code_status[order.status]
                        }]
                    }
                })
            else:
                tmp[order.package.id]['code'].append({
                    'id': order.id,
                    'code': order.code,
                    'status': code_status[order.status]
                })
        if tmp:
            resp_data = tmp.values()
        return Response(returnForm(200, "Successfully", resp_data))
    except Exception as e:
        traceback.print_exc()
        print("Purchased tickets ... ", e)
        return Response(returnForm(401, "Something happened, please try again", None))


@api_view(['POST'])
@permission_classes([IsAuthenticated])
@authentication_classes([TokenAuthentication, SessionAuthentication])
def UsedCodeViewSet(request, code_id):
    try:
        user_id = request.user.id
        user_obj = User.objects.filter(id=user_id).first()
        if not user_obj:
            return Response(returnForm(401, "User ID: {} not found".format(user_id), None))

        if not code_id or "code" not in request.data:
            return Response(returnForm(401, "Invalid input", None))

        code = request.data["code"]
        order_obj = OrderHistoryDetails.objects.filter(id=code_id, code=code).first()
        if not order_obj:
            return Response(returnForm(401, "Code ID: {} not found".format(code_id), None))

        order_obj.status = "USED"
        order_obj.save()

        return Response(returnForm(200, "Successfully", None))
    except Exception as e:
        traceback.print_exc()
        print("Package create update ... ", e)
        return Response(returnForm(401, "Something happened, please try again", None))
