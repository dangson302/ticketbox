from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.decorators import authentication_classes, permission_classes
from rest_framework.response import Response

from django.contrib.auth.models import User
from apiserver.models import Profile
from apiserver.models import Order

from apiserver.utils import Timestamp
from apiserver.utils import buildProfile
from apiserver.utils import buildOrder
from apiserver.utils import returnForm

from django.conf import settings

from datetime import datetime