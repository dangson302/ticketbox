from rest_framework.views import exception_handler

from apiserver.models import Order
from apiserver.models import Profile

from django.conf import settings

import time, datetime
from django.conf import settings

def custom_rest_exception_handler(exc, context):
    # Call REST framework's default exception handler first,
    # to get the standard error response.
    response = exception_handler(exc, context)

    if response is not None:
        response.data = {}
        errors = []
        for field, value in response.data.items():
            errors.append("{} : {}".format(field, " ".join(value)))

        response.data['code'] = 401
        response.data['data'] = errors
        response.data['message'] = str(exc)

    return response

def Timestamp(value):
    return int(time.mktime(value.timetuple()))

def returnForm(error, message, data):
    '''Return defined data'''
    return {"code": error, "message": message, "data": data}

def buildProfile(profile, type="staff"):
    data = {
        "id": profile.id,
        "type": profile.usertype.lower(),
        "username": profile.username,
        "full_name": profile.fullname,
        "phone": profile.phone,
        "email": profile.email,
    }
    if type == "staff":
        data["birthday"] = Timestamp(profile.birthday)
        data["gender"] = profile.gender
        data["avatar"] = settings.MEDIA_SERVER + "/" + str(profile.avatar)
        # data["avatar"] = "http://via.placeholder.com/800x600"# profile.avatar
        data["images"] = profile.images.split(",")
        data["description"] = profile.description
        data["rank"] = profile.rank
        data["rating"] = profile.rating
        data["comment"] = profile.comment
        data["note"] = profile.note
        data["address"] = {"id": profile.address.id, "name": profile.address.street}
        data["location"] = {"id": profile.location.id, "name": profile.location.name}
        data["height"] = profile.height
        data["weight"] = profile.weight
        data["body"] = profile.body.split(",")
        data["monthly_ammount"] ="0"
        data["schedules"] = []
        # for _schedule in profile.schedule.split(","):
        #     _s = _schedule.split(":")
        #     data["schedules"].append({"start_at": _s[0], "end_at": _s[1], "status": "free"})
    return data

def buildOrder(order, userView=True):
    # TODO: staffs: chỉ có user nhìn thấy
    # đơn giá: nhân viên # của khách
    # số điện thoại: staffs nhìn thấy khi state == Success
    data = {
        "id": order.id,
        "start_at": Timestamp(order.start_at + datetime.timedelta(hours=7)),
        "end_at": Timestamp(order.end_at + datetime.timedelta(hours=7)),
        "created_at": Timestamp(order.created_at + datetime.timedelta(hours=7)),
        "type": order.type,
        "status": order.state,
        # "user": order.user.id if order.user else "not found",
        # "cost_detail": order.cost_detail,
        "description": order.description,
        "firebaseKey": order.firebaseKey,
        "count_down_wait": 120,
        "count_down_purchasing": 300,

    }
    if userView:
        data["quantity"] = order.total_staffs
        data["staffs"] = [buildProfile(Profile.objects.filter(user=staff).first()) for staff in order.staffs.all()]
        data["cost"] = str(int(order.cost))
    else:
        print("View order's relate to :{}".format(order.staffs.first()))
        data["user"] = buildProfile(Profile.objects.filter(user=order.user).first(), "user")
        data["cost"] = str(int(int(order.cost)/order.total_staffs))

    return data