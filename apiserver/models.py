from django.db import models
from django.contrib.auth.models import User


class City(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return u'%s' % self.name


class Address(models.Model):
    street = models.CharField(max_length=300)
    city = models.ForeignKey('City', on_delete=models.CASCADE)

    def __str__(self):
        return u'%s' % self.street


class Profile(models.Model):
    """ User model"""
    PROFILE = (('USER','User'),('STAFF','Staff'))
    RANK = (('A','A'),('B','B'),('C','C'))

    user = models.OneToOneField(User, on_delete=models.CASCADE)
    usertype = models.CharField(max_length=10,choices=PROFILE,default='USER')
    username = models.CharField(max_length=300, default="")
    birthday = models.CharField(max_length=10, default="", null=True)

    gender = models.CharField(max_length=10, default="Male")
    fullname = models.CharField(max_length=300, default="")
    phone = models.CharField(max_length=20)
    email = models.EmailField(default="")
    images = models.TextField(default="")
    description = models.TextField(default="")
    rank = models.CharField(max_length=10,choices=RANK,default='A')
    rating = models.FloatField(default=0.0)
    comment = models.TextField(default="")
    note = models.TextField(default="")
    address = models.ForeignKey('Address', on_delete=models.CASCADE)
    location = models.ForeignKey('City', on_delete=models.CASCADE)
    request_organization = models.BooleanField(default=False)
    create_at = models.DateTimeField(auto_now_add=True)
    update_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return u'%s: %s (%s) - %s' % (self.usertype, self.fullname, self.username, self.phone)

    def isStaff():
        return self.usertype == "STAFF"


class Organization(models.Model):
    user = models.OneToOneField(User, on_delete=models.DO_NOTHING)
    name = models.CharField(max_length=512, default="")
    phone = models.CharField(max_length=20)
    email = models.EmailField(default="")
    images = models.TextField(default="", null=True)
    website = models.TextField(default="", null=True)
    description = models.TextField(default="", null=True)


class Category(models.Model):
    name = models.CharField(max_length=512, default="")
    description = models.TextField(default="")


# Ticket actually
class Order(models.Model):
    """Order models"""
    # STATUS = (('WAIT','WAIT'),('PURCHASING','PURCHASING'),('SUCCESS','SUCCESS'),('CANCEL','CANCEL'),('FINISH','FINISH'))
    # TYPE = (('FAST','FAST'),('NORMAL','NORMAL'))
    STATUS = (('PRIVATE', 'Private'), ('PUBLIC', 'Public'))

    title = models.CharField(max_length=150)
    description = models.TextField(default="")
    content = models.TextField(default="")
    start_at = models.DateTimeField(auto_now_add=False)
    end_at = models.DateTimeField(auto_now_add=False)
    created_at = models.DateTimeField(auto_now_add=True)
    # type = models.CharField(max_length=10, choices=TYPE)
    user = models.ForeignKey(User, related_name="User", on_delete=models.CASCADE, null=True)
    cost = models.FloatField(default=0.0, null=True)
    # state = models.CharField(max_length=20, choices=STATUS, default="OPEN")
    state = models.CharField(max_length=20, choices=STATUS, default='PUBLIC')
    location = models.ForeignKey('City', on_delete=models.DO_NOTHING, null=True)
    total = models.IntegerField(default=0, null=True)
    is_default_fee = models.BooleanField(default=True)
    tnc = models.TextField(null=True)
    faq = models.TextField(null=True)
    ads_start_at = models.DateTimeField(auto_now_add=False, null=True)
    ads_end_at = models.DateTimeField(auto_now_add=False, null=True)
    ads_total = models.FloatField(default=0.0, null=True)
    category = models.ForeignKey(Category, on_delete=models.DO_NOTHING, null=True)
    content_image = models.ImageField(upload_to='tickets/content/', null=True)
    banner = models.ImageField(upload_to='tickets', null=True)
    is_delete = models.BooleanField(default=False)

    def __str__(self):
        return u'%s - type: %s - number: %d - status: %s' % (self.id, self.type, self.total, self.state)

    class Meta:
        verbose_name = 'Ticket'
        verbose_name_plural = 'Tickets'


class Package(models.Model):
    """ package info """
    ticket = models.ForeignKey('Order',on_delete=models.CASCADE,default=None)
    title = models.CharField(max_length=150)
    content = models.TextField(default="")
    price = models.FloatField()
    quantity = models.IntegerField()
    left = models.IntegerField()
    is_delete = models.BooleanField(default=False)


class ApiVersion(models.Model):
    """ api version """
    AndroidVersionNumber = models.CharField(max_length=10)
    AndroidVersionDetail = models.CharField(max_length=200)
    IOSVersionNumber = models.CharField(max_length=10)
    IOSVersionDetail = models.CharField(max_length=200)
    minimumUserAndroidAppSupportVersion = models.CharField(max_length=10)
    minimumStaffAndroidAppSupportVersion = models.CharField(max_length=10)
    minimumUserIOSAppSupportVersion = models.CharField(max_length=10)
    minimumStaffIOSAppSupportVersion = models.CharField(max_length=10)
    releaseDate = models.DateTimeField(auto_now_add=True)


class Banner(models.Model):
    ''' Banner info '''
    BANNER_POS = (('top','top'),('left','left'),('post','post'))
    bannerType = models.CharField(max_length=100, choices=BANNER_POS, default="top")
    description = models.CharField(max_length=250)
    url = models.URLField()
    images = models.TextField(max_length=500)

    def __str__(self):
        return u'%s - %s' % (self.bannerType, self.description)

    class Meta:
        verbose_name = 'Ads banners'
        verbose_name_plural = 'Ads banner'


class OrderHistory(models.Model):
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    total = models.FloatField()
    sub_total = models.FloatField()
    tax = models.FloatField()
    coupon_discount = models.FloatField()
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = 'Order History'


class OrderHistoryDetails(models.Model):
    STATUS = (('USED', 'Used'), ('UNUSED', 'Not Used'))

    ticket = models.ForeignKey(Order, on_delete=models.DO_NOTHING)
    package = models.ForeignKey(Package, on_delete=models.DO_NOTHING, default=None)
    order = models.ForeignKey(OrderHistory, on_delete=models.DO_NOTHING)
    quantity = models.IntegerField(default=1)
    code = models.CharField(max_length=191, null=True)
    status = models.CharField(max_length=191, choices=STATUS, default='UNUSED')
    used_at = models.DateTimeField(auto_now_add=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = 'Order History Details'


class ParticipantInfo(models.Model):
    order_history = models.ForeignKey(OrderHistoryDetails, on_delete=models.DO_NOTHING)
    participant_name = models.CharField(max_length=512, default=None)
    contact_number = models.CharField(max_length=512, default=None)
    email = models.EmailField(default=None)
    ic_number = models.CharField(max_length=512, default=None)
    emergency_contact = models.CharField(max_length=512, default=None)
    other_info = models.TextField(default=None)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = 'Participant Info'
