import time

from rest_framework import serializers
from apiserver.models import Order


class TimestampField(serializers.Field):
    def to_representation(self, value):
        return int(time.mktime(value.timetuple()))

class ToArrayJson(serializers.Field):
    def to_representation(self, value):
        return value.split(",")

class EmployeeSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=300)
    stage_name = serializers.CharField(max_length=300)
    stage_name_only = serializers.BooleanField(default=False)
    isFemale = serializers.BooleanField(default=True)
    birthday = TimestampField(serializers.DateTimeField())
    # address = serializers.ForeignKey('Address')
    rating = serializers.FloatField()
    comment = serializers.CharField()
    title_image = serializers.URLField()
    detail_images = ToArrayJson(serializers.CharField(max_length=500))
    description = serializers.CharField()
    detail = serializers.CharField()
    reg_date = TimestampField(serializers.DateTimeField())

class UserSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=300)
    isFemale = serializers.BooleanField(default=True)
    birthday = serializers.DateTimeField()
    # address = serializers.ForeignKey('Address')
    mobile = serializers.CharField(max_length=15)
    avatar = serializers.URLField()
    description = serializers.CharField()
    reg_date = TimestampField(serializers.DateTimeField())

class TicketSerializer(serializers.Serializer):
    status = serializers.CharField()
    detail = serializers.CharField()
    # user = serializers.ForeignKey('User')
    # employee = serializers.ManyToManyField(Employee)
    start_time = TimestampField(serializers.DateTimeField())
    end_time = TimestampField(serializers.DateTimeField())

    created_time = serializers.DateTimeField()


class AddToCardSerializer(serializers.Serializer):
    ticket_id = serializers.IntegerField(required=True, min_value=1)
    package_id = serializers.IntegerField(required=True, min_value=1)
    quantity = serializers.IntegerField(required=True, min_value=0)


class ParticipantInfoSerializer(serializers.Serializer):
    name = serializers.CharField()
    contact_number = serializers.CharField()
    email = serializers.CharField()
    ic_number = serializers.CharField()
    emergency_contact = serializers.CharField()
    other_info = serializers.ListField()


class ProfileSerializer(serializers.Serializer):
    fullname = serializers.CharField()
    email = serializers.CharField()
    phone = serializers.CharField()
    birthday = serializers.CharField()


class OrganizationSerializer(serializers.Serializer):
    name = serializers.CharField()
    email = serializers.CharField()
    phone = serializers.CharField()
    website = serializers.CharField(allow_blank=True, allow_null=True)
    description = serializers.CharField(allow_blank=True, allow_null=True)


class PackageSerializer(serializers.Serializer):
    package_id = serializers.IntegerField()
    title = serializers.CharField()
    price = serializers.FloatField(min_value=0.0)
    quantity = serializers.IntegerField(min_value=0)
    content = serializers.CharField(allow_blank=True, allow_null=True)


class TicketSerializer(serializers.Serializer):
    ticket_id = serializers.IntegerField()
    title = serializers.CharField()
    start_at = serializers.CharField()
    description = serializers.CharField(allow_blank=True, allow_null=True)
    content = serializers.CharField(allow_blank=True, allow_null=True)
    state = serializers.ChoiceField(choices=dict(Order.STATUS))
    faq = serializers.CharField(allow_blank=True, allow_null=True)
    tnc = serializers.CharField(allow_blank=True, allow_null=True)
    is_default_fee = serializers.BooleanField()
    category_id = serializers.IntegerField()
    location_id = serializers.IntegerField()
    ads_start_at = serializers.CharField()
    ads_end_at = serializers.CharField()
    ads_total = serializers.FloatField(min_value=0.0)
