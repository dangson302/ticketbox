# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('apiserver', '0008_auto_20190916_0219'),
    ]

    operations = [
        migrations.CreateModel(
            name='OrderHistory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('total', models.FloatField()),
                ('sub_total', models.FloatField()),
                ('tax', models.FloatField()),
                ('coupon_discount', models.FloatField()),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('ticket', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='apiserver.Order')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Order History',
            },
        ),
        migrations.CreateModel(
            name='OrderHistoryDetails',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('quantity', models.IntegerField(default=1)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('order', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='apiserver.OrderHistory')),
                ('ticket', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='apiserver.Order')),
            ],
            options={
                'verbose_name': 'Order History Details',
            },
        ),
        migrations.CreateModel(
            name='ParticipantInfo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('participant_name', models.CharField(max_length=512, default=None)),
                ('contact_number', models.CharField(max_length=512, default=None)),
                ('email', models.EmailField(max_length=254, default=None)),
                ('ic_number', models.CharField(max_length=512, default=None)),
                ('emergency_contact', models.CharField(max_length=512, default=None)),
                ('other_info', models.TextField(default=None)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('order_history', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='apiserver.OrderHistoryDetails')),
            ],
            options={
                'verbose_name': 'Participant Info',
            },
        ),
    ]
