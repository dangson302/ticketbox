# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('apiserver', '0009_orderhistory_orderhistorydetails_participantinfo'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='orderhistory',
            name='ticket',
        ),
    ]
