# Generated by Django 2.2.3 on 2019-09-25 05:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('apiserver', '0021_auto_20190924_2218'),
    ]

    operations = [
        migrations.AddField(
            model_name='package',
            name='is_delete',
            field=models.BooleanField(default=False),
        ),
    ]
