from django.apps import AppConfig


class ApiserverConfig(AppConfig):
    name = 'apiserver'
    verbose_name = 'Content manager'
