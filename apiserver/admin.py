from django.contrib import admin
from apiserver.models import City
from apiserver.models import Address
from apiserver.models import Profile
from apiserver.models import Order
from apiserver.models import ApiVersion
from apiserver.models import Banner
from apiserver.models import Package

# Register your models here.

class ProfileFilter(admin.SimpleListFilter):
    # Human-readable title which will be displayed in the
    # right admin sidebar just above the filter options.
    title = 'User type'
    parameter_name = 'usertype'

    def lookups(self, request, model_admin):
        return (
            ('USER', 'User'),
            ('GUESS', 'Guess'),
        )

    def queryset(self, request, queryset):
        if self.value() == 'USER':
            return queryset.filter(usertype="USER")
        if self.value() == 'GUESS':
            return queryset.filter(usertype="GUESS")

class ProfileAdmin(admin.ModelAdmin):
    list_filter = [ProfileFilter]
    search_fields = ('fullname','username', 'phone', 'email')

admin.site.register(City)
admin.site.register(Address)
admin.site.register(Profile, ProfileAdmin)
admin.site.register(Order)
# admin.site.register(ApiVersion)
admin.site.register(Banner)
admin.site.register(Package)

admin.site.site_header = 'My administration'
