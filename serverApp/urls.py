"""serverApp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.urls import path
from django.conf.urls import include
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings

from rest_framework import routers
from apiserver import views

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path(r'jet/', include('jet.urls', 'jet')),  # Django JET URLS
    path(r'jet/dashboard/', include('jet.dashboard.urls', 'jet-dashboard')),  # Django JET dashboard URLS
    path(r'admin/', admin.site.urls),
    path(r'api/v1.0/home', views.HomeViewSet),
    path(r'api/v1.0/update', views.VersionViewSet),
    path(r'api/v1.0/locations', views.LocationViewSet),
    path(r'api/v1.0/user/register', views.RegisterViewSet),
    path(r'api/v1.0/user/login', views.LoginViewSet),
    path(r'api/v1.0/user/logout', views.LogoutViewSet),
    path(r'api/v1.0/user/resetpass', views.ResetPassViewSet),
    path(r'api/v1.0/user/forgot', views.ForgotViewSet),
    path(r'api/v1.0/user/profile', views.ProfileViewSet),
    path(r'api/v1.0/user/profile/update', views.UpdateProfileViewSet),
    path(r'api/v1.0/user/organization', views.OrganizationViewSet),
    path(r'api/v1.0/user/request/organization', views.RequestOrganizationViewSet),
    path(r'api/v1.0/user/update/organization', views.UpdateOrganizationViewSet),
    path(r'api/v1.0/search', views.SearchMany),
    path(r'api/v1.0/tickets', views.TicketsView),
    path(r'api/v1.0/tickets/purchased', views.TicketsPurchasedViewSet),
    path(r'api/v1.0/ticket/cart/add', views.AddToCartViewSet),
    path(r'api/v1.0/ticket/cart/list', views.CartListViewSet),
    path(r'api/v1.0/ticket/cart/checkout', views.CartCheckoutViewSet),
    path(r'api/v1.0/ticket/cart/make-payment', views.CartMakePaymentViewSet),
    path(r'api/v1.0/ticket/<int:ticket_id>', views.TicketView),
    path(r'api/v1.0/ticket/<int:ticket_id>/packages/purchased', views.PackagesPurchasedViewSet),
    path(r'api/v1.0/organizer/ticket/list', views.OrganizerTicketsViewSet),
    path(r'api/v1.0/organizer/ticket/create', views.OrganizerTicketCreateUpdateViewSet),
    path(r'api/v1.0/organizer/ticket/<int:ticket_id>/detail', views.OrganizerTicketDetailViewSet),
    path(r'api/v1.0/organizer/ticket/<int:ticket_id>/remove', views.OrganizerTicketRemoveViewSet),
    path(r'api/v1.0/organizer/ticket/<int:ticket_id>/packages', views.PackagesViewSet),
    path(r'api/v1.0/organizer/ticket/<int:ticket_id>/package/create', views.PackageCreateUpdateViewSet),
    path(r'api/v1.0/organizer/ticket/<int:ticket_id>/package/<int:package_id>/detail', views.PackageDetailViewSet),
    path(r'api/v1.0/organizer/ticket/<int:ticket_id>/package/<int:package_id>/remove', views.PackageDeleteViewSet),

    path(r'api/v1.0/code/<int:code_id>/used', views.UsedCodeViewSet),
]
